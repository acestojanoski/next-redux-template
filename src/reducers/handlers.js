import * as apiReducers from '../api/api-reducers';

const handlers = {
    ...apiReducers,
};

export default handlers;
