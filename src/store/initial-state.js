import { fromJS } from 'immutable';

const initialState = {
    apiCalls: {},
};

export default fromJS(initialState);
